<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * MathObservation
 *
 */
class MathObservation extends Model
{
    public $x;
    public $y;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['x', 'y'], 'required'],
            [['x', 'y'], 'integer', 'integerOnly' => false,]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'x' => 'x',
            'y' => 'y',
        ];
    }
}
