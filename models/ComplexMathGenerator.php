<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ComplexMathGenerator
 *
 */
class ComplexMathGenerator extends Model
{
    public $count;

    public $mathObservations = [];
    public $xHorizontal = [];
    public $xVertical = [];
    public $xtx = [];
    public $xty = [];
    public $yt = [];
    public $e = [];
    public $a = [];
    public $aMiddle = 0;
    public $yMiddle = 0;
    public $x1Middle = 0;
    public $x2Middle = 0;
    public $el1 = 0;
    public $el2 = 0;
    public $determinant = 0;
    public $determinantCoeff = 0;
    public $a11 = 0;
    public $a12 = 0;
    public $a13 = 0;
    public $a21 = 0;
    public $a22 = 0;
    public $a23 = 0;
    public $a31 = 0;
    public $a32 = 0;
    public $a33 = 0;
    public $aArray = [];
    public $aArrayRevert = [];
    public $aValues = [];
    public $mathObservationsSum = [
        'sumx1'=>0,
        'sumx2'=>0,
    ];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['count'], 'required'],
            [['count'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'count' => 'Кол-во наблюдений',
        ];
    }

    public function calculate()
    {
        $i = 0;
        $ySum = 0;
        $x1Sum = 0;
        $x2Sum = 0;
        foreach ($this->mathObservations as $index => $mathObservation) {
            for ($i=0; $i<=2; $i++){
                if (!isset($this->xHorizontal[$i][$index])){
                    $this->xHorizontal[$i][$index]=0;
                }
                if (!isset($this->xVertical[$index][$i])){
                    $this->xVertical[$index][$i]=0;
                }
            }

            $this->xHorizontal[0][$index] = 1;
            $this->xHorizontal[1][$index] = $mathObservation['x1'];
            $this->xHorizontal[2][$index] = $mathObservation['x2'];

            $this->xVertical[$index][0] = 1;
            $this->xVertical[$index][1] = $mathObservation['x1'];
            $this->xVertical[$index][2] = $mathObservation['x2'];

            $this->mathObservationsSum['sumx1'] += $mathObservation['x1'];
            $this->mathObservationsSum['sumx2'] += $mathObservation['x2'];

            $ySum += $mathObservation['y'];
            $x1Sum += $mathObservation['x1'];
            $x2Sum += $mathObservation['x2'];
        }
        $this->yMiddle = $ySum / count($this->mathObservations);
        $this->x1Middle = $x1Sum / count($this->mathObservations);
        $this->x2Middle = $x2Sum / count($this->mathObservations);

        foreach ($this->xHorizontal as $index => $row) {
            foreach ($row as $key => $value) {
                for ($i=0; $i<=2; $i++) {
                    if (!isset($this->xtx[$index][$i])){
                        $this->xtx[$index][$i]=0;
                    }
                    $this->xtx[$index][$i] += $value * $this->xVertical[$key][$i];
                }
            }
        }

        $this->determinant = round(($this->xtx[0][0]*$this->xtx[1][1]*$this->xtx[2][2]+$this->xtx[0][1]*$this->xtx[2][0]*$this->xtx[1][2]+$this->xtx[0][2]*$this->xtx[2][1]*$this->xtx[1][0]-$this->xtx[0][2]*$this->xtx[1][1]*$this->xtx[2][0]-$this->xtx[0][0]*$this->xtx[2][1]*$this->xtx[1][2]-$this->xtx[0][1]*$this->xtx[1][0]*$this->xtx[2][2]), 2);
        ($this->determinant) ? $this->determinantCoeff = 1 / $this->determinant : $this->determinantCoeff = 0;

        $this->a11 = round(pow(-1, 1+1) * ($this->xtx[1][1]*$this->xtx[2][2]-$this->xtx[1][2]*$this->xtx[2][1]), 2);
        $this->a12 = round(pow(-1, 1+2) * ($this->xtx[1][0]*$this->xtx[2][2]-$this->xtx[1][2]*$this->xtx[2][0]), 2);
        $this->a13 = round(pow(-1, 1+3) * ($this->xtx[1][0]*$this->xtx[2][1]-$this->xtx[1][1]*$this->xtx[2][0]), 2);

        $this->a21 = round(pow(-1, 2+1) * ($this->xtx[0][1]*$this->xtx[2][2]-$this->xtx[0][2]*$this->xtx[2][1]), 2);
        $this->a22 = round(pow(-1, 2+2) * ($this->xtx[0][0]*$this->xtx[2][2]-$this->xtx[0][2]*$this->xtx[2][0]), 2);
        $this->a23 = round(pow(-1, 2+3) * ($this->xtx[0][0]*$this->xtx[2][1]-$this->xtx[0][1]*$this->xtx[2][0]), 2);

        $this->a31 = round(pow(-1, 3+1) * ($this->xtx[0][1]*$this->xtx[1][2]-$this->xtx[0][2]*$this->xtx[1][1]), 2);
        $this->a32 = round(pow(-1, 3+2) * ($this->xtx[0][0]*$this->xtx[1][2]-$this->xtx[0][2]*$this->xtx[1][0]), 2);
        $this->a33 = round(pow(-1, 3+3) * ($this->xtx[0][0]*$this->xtx[1][1]-$this->xtx[0][1]*$this->xtx[1][0]), 2);

        array_push($this->aArray, [$this->a11, $this->a21, $this->a31]);
        array_push($this->aArray, [$this->a12, $this->a22, $this->a32]);
        array_push($this->aArray, [$this->a13, $this->a23, $this->a33]);

        foreach ($this->aArray as $index=>$array){
            foreach ($array as $key=>$value){
                if (!isset($this->aArrayRevert[$index][$key])){
                    $this->aArrayRevert[$index][$key]=0;
                }
                $this->aArrayRevert[$index][$key]=$value * $this->determinantCoeff;
            }
        }

        foreach ($this->xHorizontal as $index=>$array){
            foreach ($array as $key=>$value){
                if (!isset($this->xty[$index])){
                    $this->xty[$index]=0;
                }
                $this->xty[$index] += $value * $this->mathObservations[$key]['y'];
            }
        }

        foreach ($this->aArrayRevert as $index=>$array){
            foreach ($array as $key=>$value){
                if (!isset($this->aValues[$index])){
                    $this->aValues[$index]=0;
                }
                $this->aValues[$index] += $value * $this->xty[$key];
            }
        }

        foreach ($this->xVertical as $index=>$array){
            foreach ($array as $key=>$value){
                if (!isset($this->yt[$index])){
                    $this->yt[$index]=0;
                }
                $this->yt[$index] += $value * $this->aValues[$key];
            }
        }

        foreach ($this->yt as $key=>$value){
            if (!isset($this->e[$key])){
                $this->e[$key]=0;
            }
            $this->e[$key] = $value - $this->mathObservations[$key]['y'];
        }


        $aSum = 0;
        foreach ($this->e as $key=>$value){
            if (!isset($this->a[$key])){
                $this->a[$key]=0;
            }
            $this->a[$key] = abs($value / $this->mathObservations[$key]['y']) * 100;
            $aSum += $this->a[$key];
        }
        $this->aMiddle = $aSum/count($this->a);

        $this->el1 = ($this->aValues[1]+$this->aValues[2])*$this->x1Middle/$this->yMiddle;
        $this->el2 = ($this->aValues[1]+$this->aValues[2])*$this->x2Middle/$this->yMiddle;
    }
}
