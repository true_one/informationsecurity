<?php

namespace app\models;

use Yii;

/**
 * ComplexPasswordGenerator
 *
 */
class ComplexPasswordGenerator extends CommonModel
{
    public $username;

    const ALPHABET_LATIN_START = 97;
    const ALPHABET_LATIN_END = 122;
    const ALPHABET_UPPER_LATIN_START = 65;
    const ALPHABET_UPPER_LATIN_END = 90;
    const ALPHABET_NUMBER_START = 48;
    const ALPHABET_NUMBER_END = 57;
    const ALPHABET_CHAR_START = 33;
    const ALPHABET_CHAR_END = 42;
    const ALPHABET_CYRILLIC_START = 224;
    const ALPHABET_CYRILLIC_END = 255;
    const ALPHABET_UPPER_CYRILLIC_START = 192;
    const ALPHABET_UPPER_CYRILLIC_END = 223;
    //iconv("CP1251", "UTF-8",chr($cyrillic_number_range))

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['username'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
        ];
    }

    /**
     * @return array
     */
    public function calculate()
    {
        $return = [];
        if ($this->validate()){
            array_push($return, [
                'variant' => "№1",
                'length'=>6,
                'description' =>
                    "1. \({b}_{1}\), \({b}_{2}\) - случайные заглавные буквы английского алфавита. <br>
                     2. \({b}_{3}\) = \(N^2\) mod 10 (где mod 10 – остаток от деления числа на 10). <br>
                     3. \({b}_{4}\) - случайная цифра. <br>
                     4. \({b}_{5}\) - случайный символ из множества {!,”,#,$,%,&,’,(,),*}. <br>
                     5. \({b}_{6}\) - случайная малая буква английского алфавита.",
                'password' => $this->generatePasswordFirst(),
            ]);
            array_push($return, [
                'variant' => "№2",
                'length'=>7,
                'description' =>
                    "1. \({b}_{1}\), \({b}_{2}\), \({b}_{3}\) - случайные малые буквы английского алфавита. <br>
                     1. \({b}_{4}\), \({b}_{5}\) - случайные заглавные буквы английского алфавита. <br>
                     1. \({b}_{6}\), \({b}_{7}\) - двузначное число, равное \(N^4\) mod 100. (Если остаток – однозначное число, то \({b}_{6}\) = 0).",
                'password' => $this->generatePasswordSecond(),
            ]);
            array_push($return, [
                'variant' => "№3",
                'length'=>8,
                'description' =>
                    "1. \({b}_{1}\), \({b}_{2}\), \({b}_{3}\) - случайные цифры. <br>
                     2. \({b}_{4}\), \({b}_{5}\) - случайные символы из множества {!,”,#,$,%,&,’,(,),*}. <br>
                     3. \({b}_{7}\) - случайная заглавная буква английского алфавита. <br>
                     4. \({b}_{8}\) - P –ая по счету малая буква английского алфавита, где P = \(N^2\) mod 10 + \(N^3\) mod 10 + 1.",
                'password' => $this->generatePasswordThird(),
            ]);
            array_push($return, [
                'variant' => "№4",
                'length'=>9,
                'description' =>
                    "1. \({b}_{1}\), ..., \({b}_{1+Q}\) - случайные символы из множества {!,”,#,$,%,&,’,(,),*}, где Q = N mod 5. <br>
                     2. Оставшиеся символы пароля, кроме \({b}_{9}\), - случайные малые буквы английского алфавита. <br>
                     3. \({b}_{9}\) - случайная цифра.",
                'password' => $this->generatePasswordForth(),
            ]);
        }

        return $return;
    }

    /**
     * @return string
     */
    public function generatePasswordFirst()
    {
        $count = mb_strlen($this->username, 'UTF-8');
        $length = 6;

        $password = '';
        for ($i=0; $i<$length; $i++)
        {
            switch($i+1)
            {
                case 3:
                    $password .= pow($count, 2) % 10; // b3 - N в квадрате mod 10
                    break;
                case 4:
                    $password .= self::getChar(self::ALPHABET_NUMBER_START, self::ALPHABET_NUMBER_END); // b4 - случайная цифра
                    break;
                case 5:
                    $password .= self::getChar(self::ALPHABET_CHAR_START, self::ALPHABET_CHAR_END); // b5 - случайный символ из множества {!,”,#,$,%,&,’,(,),*}
                    break;
                case 6:
                    $password .= self::getChar(self::ALPHABET_LATIN_START, self::ALPHABET_LATIN_END); // b6 - случайная малая буква английского алфавита
                    break;
                default:
                    $password .= self::getChar(self::ALPHABET_UPPER_LATIN_START, self::ALPHABET_UPPER_LATIN_END); // b1,b2 - случайные заглавные буквы английского алфавита
            }
        }
        return $password;
    }

    /**
     * @return string
     */
    public function generatePasswordSecond()
    {
        $count = mb_strlen($this->username, 'UTF-8');
        $length = 7;

        $password = '';
        for ($i=0; $i<$length; $i++)
        {
            if (self::inRange($i+1, 1, 3))
            {
                $password .= self::getChar(self::ALPHABET_LATIN_START, self::ALPHABET_LATIN_END); // b1,b2,b3 - случайные малые буквы английского алфавита
            }
            elseif(self::inRange($i+1, 4, 5))
            {
                $password .= self::getChar(self::ALPHABET_UPPER_LATIN_START, self::ALPHABET_UPPER_LATIN_END); // b4,b5 - случайные заглавные буквы английского алфавита
            }
            elseif(self::inRange($i+1, 6, 7))
            {
                $password .= str_pad(pow($count, 4) % 100, 2, "0", STR_PAD_LEFT); // b6,b7 - двузначное число, равное N в 4ой степени mod 100 . (Если остаток – однозначное число, то b6=0)
                break;
            }
        }
        return $password;
    }

    /**
     * @return string
     */
    public function generatePasswordThird()
    {
        $count = mb_strlen($this->username, 'UTF-8');
        $length = 8;

        $password = '';
        for ($i=0; $i<$length; $i++)
        {
            if (self::inRange($i+1, 1, 3))
            {
                $password .= self::getChar(self::ALPHABET_NUMBER_START, self::ALPHABET_NUMBER_END); // b1,b2,b3 - случайные цифры
            }
            elseif(self::inRange($i+1, 4, 6))
            {
                $password .= self::getChar(self::ALPHABET_CHAR_START, self::ALPHABET_CHAR_END); // b4,b5 - случайные символы из множества {!,”,#,$,%,&,’,(,),*}
                // b6 - в описании не нашел, поэтому этот символ тоже сюда
            }
            elseif(self::inRange($i+1, 7, 7))
            {
                $password .= self::getChar(self::ALPHABET_UPPER_LATIN_START, self::ALPHABET_UPPER_LATIN_END); // b7 - случайная заглавная буква английского алфавита
            }
            elseif(self::inRange($i+1, 8, 8))
            {
                $p = pow($count, 2) % 10 + pow($count, 3) % 10 + 1;
                $password .= chr(self::ALPHABET_LATIN_START + $p - 1); // b8 - P –ая по счету малая буква английского алфавита, где P = N (в квадрате) mod 10 + N (в кубе) mod 10 + 1
            }
        }
        return $password;
    }

    /**
     * @return string
     */
    public function generatePasswordForth()
    {
        $count = mb_strlen($this->username, 'UTF-8');
        $length = 9;
        $q = $count % 5;
        $password = '';
        for ($i=0; $i<$length; $i++)
        {
            if(self::inRange($i+1, 9, 9))
            {
                $password .= self::getChar(self::ALPHABET_NUMBER_START, self::ALPHABET_NUMBER_END); // b9 - случайная цифра.
            }
            elseif (self::inRange($i+1, 1, $q))
            {
                $password .= self::getChar(self::ALPHABET_CHAR_START, self::ALPHABET_CHAR_END); // b1 .. bQ - случайные символы из множества {!,”,#,$,%,&,’,(,),*}
            }
            else
            {
                $password .= self::getChar(self::ALPHABET_LATIN_START, self::ALPHABET_LATIN_END); // Оставшиеся символы пароля, кроме b9, - случайные малые буквы английского алфавита.
            }
        }
        return $password;
    }
}
