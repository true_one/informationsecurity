<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ComplexMathObservation
 *
 */
class ComplexMathObservation extends Model
{
    public $x1;
    public $x2;
    public $x3;
    public $y;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['x1', 'x2', 'x3', 'y'], 'required'],
            [['x1', 'x2', 'x3', 'y'], 'integer', 'integerOnly' => false,]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'x1' => 'x1',
            'x2' => 'x2',
            'x3' => 'x3',
            'y' => 'y',
        ];
    }
}
