<?php

namespace app\models;

use Yii;

/**
 * PasswordGenerator
 *
 * @property string[] $VUnits
 * @property string[] $VUnitsLabels
 * @property string[] $TUnits
 * @property string[] $TUnitsLabels
 *
 */
class PasswordGenerator extends CommonModel
{
    public $VValue;
    public $VUnit;
    public $TValue;
    public $TUnit;
    public $PValue = 10;
    public $PUnit;
    public $count = 5;

    const ALPHABET_LATIN = 26;
    const ALPHABET_LATIN_NUMBER = 36;
    const ALPHABET_LATIN_NUMBER_CHAR = 46;
    const ALPHABET_LATIN_NUMBER_CHAR_BIG_LATIN = 72;

    const MIN_L_VALUE = 6;

    const V_UNIT_PER_MINUTE = 1;
    const V_UNIT_PER_DAY = 1440; // 1*60*24;
    const V_UNIT_PER_WEEK = 10080; // 1*60*24*7;

    const T_UNIT_DAY = 1440; // 1*60*24;
    const T_UNIT_WEEK = 10080; // 1*60*24*7;
    const T_UNIT_MONTH = 43920; // 1*60*24*30.5;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['VValue', 'VUnit', 'TValue', 'TUnit', 'PValue', 'PUnit', 'count'], 'required'],
            [['VValue', 'VUnit', 'TValue', 'TUnit', 'PValue', 'PUnit', 'count'], 'integer'],
            ['VUnit', 'in', 'range'=>$this->getVUnits()],
            ['TUnit', 'in', 'range'=>$this->getTUnits()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'VValue' => 'V-скорость',
            'VUnit' => 'ед. измерения скорости',
            'TValue' => 'T-время действия',
            'TUnit' => 'ед. измерения времени',
            'PValue' => 'P-коэфицент',
            'PUnit' => 'степень P-коэфицента (с учетом знака «-»)',
            'count' => 'Кол-во паролей',
        ];
    }

    /**
     * @return array
     */
    public static function getVUnits()
    {
        return [
            self::V_UNIT_PER_MINUTE => self::V_UNIT_PER_MINUTE,
            self::V_UNIT_PER_DAY => self::V_UNIT_PER_DAY,
            self::V_UNIT_PER_WEEK => self::V_UNIT_PER_WEEK,
        ];
    }

    /**
     * @return array
     */
    public static function getVUnitsLabels()
    {
        return [
            self::V_UNIT_PER_MINUTE => 'Паролей в минуту',
            self::V_UNIT_PER_DAY => 'Паролей в день',
            self::V_UNIT_PER_WEEK => 'Паролей в неделю',
        ];
    }


    /**
     * @return array
     */
    public static function getTUnits()
    {
        return [
            self::T_UNIT_DAY => self::T_UNIT_DAY,
            self::T_UNIT_WEEK => self::T_UNIT_WEEK,
            self::T_UNIT_MONTH => self::T_UNIT_MONTH,
        ];
    }

    /**
     * @return array
     */
    public static function getTUnitsLabels()
    {
        return [
            self::T_UNIT_DAY => 'День',
            self::T_UNIT_WEEK => 'Неделя',
            self::T_UNIT_MONTH => 'Месяц',
        ];
    }

    /**
     * @return array
     */
    public function getPossibleCharactersCount()
    {
        return [
            self::ALPHABET_LATIN, //малые латинские буквы
            self::ALPHABET_LATIN_NUMBER, //малые латинские буквы, символы цифр [0-9]
            self::ALPHABET_LATIN_NUMBER_CHAR, //малые латинские буквы, символы цифр [0-9], символы знаков [! - 33, “ – 34, # - 35, $ - 36, % - 37, & - 38, ‘ – 39, ( - 40, ) – 41, * - 42]
            self::ALPHABET_LATIN_NUMBER_CHAR_BIG_LATIN, //малые и большие латинские буквы, символы цифр [0-9], символы знаков [! - 33, “ – 34, # - 35, $ - 36, % - 37, & - 38, ‘ – 39, ( - 40, ) – 41, * - 42]
        ];
    }
    /**
     * @param $value
     * @return array
     */
    public function getPossibleCharactersCountLabel($value)
    {
        switch ($value)
        {
            case self::ALPHABET_LATIN:
                return 'Малые латинские буквы (26 симв.)';
                break;
            case self::ALPHABET_LATIN_NUMBER:
                return 'Малые латинские буквы, символы цифр [0-9] (36 симв.)';
                break;
            case self::ALPHABET_LATIN_NUMBER_CHAR:
                return 'Малые латинские буквы, символы цифр [0-9], символы знаков [!, “, #, $, %, &, ‘, (, ), *] (46 симв.)';
                break;
            case self::ALPHABET_LATIN_NUMBER_CHAR_BIG_LATIN:
                return 'Малые и большие латинские буквы, символы цифр [0-9], символы знаков [!, “, #, $, %, &, ‘, (, ), *] (72 симв.)';
                break;
            default:
                return $value;
        }
    }

    /**
     * @return array
     */
    public function calculate()
    {
        if (($values = $this->getValuesForCalculation())){
            $s = $values->v * $values->t / $values->p;
            $return = [];
            foreach ($this->getPossibleCharactersCount() as $a)
            {
                $sTemp = 0;
                $l = self::MIN_L_VALUE;
                while ($sTemp < $s)
                {
                    $sTemp = pow($a, $l);
                    $l++;
                }
                array_push($return, [
                    'A' => $this->getPossibleCharactersCountLabel($a),
                    'L' => $l-1,
                    'S' => $sTemp,
                    'generatedPassword' => $this->generatePasswords($a, $l-1, $this->count),
                ]);
            }
            return $return;
        }

        return false;
    }

    /**
     * @return object|false
     */
    public function getValuesForCalculation()
    {
        if ($this->validate()){
            return (object)[
                'v' => $this->VValue * $this->VUnit,
                't' => $this->TValue * $this->TUnit,
                'p' => pow($this->PValue, $this->PUnit),
            ];
        }

        return false;
    }

    /**
     * @param $avalue
     * @param $lvalue
     * @param $count
     * @return array
     */
    public function generatePasswords($avalue=self::ALPHABET_LATIN, $lvalue=self::MIN_L_VALUE, $count=5)
    {
        $return = [];
        for ($i=0; $i<$count; $i++)
        {
            $generatedPassword = '';
            for ($j=0; $j<$lvalue; $j++)
            {
                $generatedPassword .= chr(self::getRandom($avalue));
            }
            array_push($return, $generatedPassword);
        }
        return $return;
    }

    /**
     * @param $avalue
     * @return integer
     */
    public static function getRandom($avalue=self::ALPHABET_LATIN)
    {
        switch ($avalue)
        {
            case self::ALPHABET_LATIN:
                $random = mt_rand(97, 122);
                while (!self::inRange($random, 97, 122))
                {
                    $random = mt_rand(97, 122);
                }
                break;
            case self::ALPHABET_LATIN_NUMBER:
                $random = mt_rand(48, 122);
                while (!self::inRange($random, 48, 57) && !self::inRange($random, 97, 122))
                {
                    $random = mt_rand(48, 122);
                }
                break;
            case self::ALPHABET_LATIN_NUMBER_CHAR:
                $random = mt_rand(33, 122);
                while (!self::inRange($random, 33, 42) && !self::inRange($random, 48, 57) && !self::inRange($random, 97, 122))
                {
                    $random = mt_rand(33, 122);
                }
                break;
            case self::ALPHABET_LATIN_NUMBER_CHAR_BIG_LATIN:
                $random = mt_rand(33, 122);
                while (!self::inRange($random, 33, 42) && !self::inRange($random, 48, 57) && !self::inRange($random, 65, 90) && !self::inRange($random, 97, 122))
                {
                    $random = mt_rand(33, 122);
                }
                break;
            default:
                $random = mt_rand(97, 122);
                while (!self::inRange($random, 97, 122))
                {
                    $random = mt_rand(97, 122);
                }
        }
        return $random;
    }
}
