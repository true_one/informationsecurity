<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * MathGenerator
 *
 */
class MathGenerator extends Model
{
    public $count;

    public $mathObservations = [];
    public $mathObservationsSum = [
        'sumx'=>0,
        'sumy'=>0,
        'avx'=>0,
        'avy'=>0,
        'sumxy'=>0,
        'sumx2'=>0,
        'avmodule'=>0,
    ];
    public $mathObservationsDeltas = [
        'delta'=>0,
        'adelta'=>0,
        'bdelta'=>0,
        'a'=>0,
        'b'=>0,
        'elastic'=>0,
    ];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['count'], 'required'],
            [['count'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'count' => 'Кол-во наблюдений',
        ];
    }

    public function calculate()
    {
        $i = 0;
        foreach ($this->mathObservations as $mathObservation)
        {
            $this->mathObservations[$i] = array_merge(
                $this->mathObservations[$i],
                [
                    'xy'=>$mathObservation['x'] * $mathObservation['y'],
                    'x2'=>pow($mathObservation['x'],2),
                    'yx'=>0,
                    'e'=>0,
                    'module'=>0,
                ]
            );
            $this->mathObservationsSum['sumx'] += $mathObservation['x'];
            $this->mathObservationsSum['sumy'] += $mathObservation['y'];
            $this->mathObservationsSum['sumxy'] += $this->mathObservations[$i]['xy'];
            $this->mathObservationsSum['sumx2'] += $this->mathObservations[$i]['x2'];
            $i++;
        }
        $this->mathObservationsSum['avx'] = $this->mathObservationsSum['sumx'] / $i;
        $this->mathObservationsSum['avy'] = $this->mathObservationsSum['sumy'] / $i;

        $this->mathObservationsDeltas['delta'] = $this->count * $this->mathObservationsSum['sumx2'] - pow($this->mathObservationsSum['sumx'], 2);
        $this->mathObservationsDeltas['adelta'] = $this->mathObservationsSum['sumy'] * $this->mathObservationsSum['sumx2'] - $this->mathObservationsSum['sumx'] * $this->mathObservationsSum['sumxy'];
        $this->mathObservationsDeltas['bdelta'] = $this->count * $this->mathObservationsSum['sumxy'] - $this->mathObservationsSum['sumy'] * $this->mathObservationsSum['sumx'];
        $this->mathObservationsDeltas['a'] = $this->mathObservationsDeltas['adelta'] / $this->mathObservationsDeltas['delta'];
        $this->mathObservationsDeltas['b'] = $this->mathObservationsDeltas['bdelta'] / $this->mathObservationsDeltas['delta'];
        $this->mathObservationsDeltas['elastic'] = $this->mathObservationsDeltas['b'] * $this->mathObservationsSum['avx'] / $this->mathObservationsSum['avy'];

        $i = 0;
        $summodule = 0;
        foreach ($this->mathObservations as $mathObservation)
        {
            $this->mathObservations[$i]['yx'] = $this->mathObservationsDeltas['a'] + $this->mathObservationsDeltas['b'] * $mathObservation['x'];
            $this->mathObservations[$i]['e'] = $mathObservation['y'] - $this->mathObservations[$i]['yx'];
            $this->mathObservations[$i]['module'] = abs($this->mathObservations[$i]['e'] / $mathObservation['y'] * 100);
            $summodule += $this->mathObservations[$i]['module'];
            $i++;
        }
        $this->mathObservationsSum['avmodule'] = $summodule / $i;
    }
}
