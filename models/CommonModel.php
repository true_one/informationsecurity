<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ComplexPasswordGenerator
 *
 */
class CommonModel extends Model
{
    const START_INDEX = 32;
    const END_INDEX = 126;

    /**
     * @param $startIndex integer
     * @param $endIndex integer
     * @return array
     */
    public static function getDictionary($startIndex=self::START_INDEX, $endIndex=self::END_INDEX)
    {
        $dictionary = [];
        for ($i = $startIndex; $i <= $endIndex; $i++) {
            array_push($dictionary, chr($i));
        }
        return $dictionary;
    }
    /**
     * @param $startIndex integer
     * @param $endIndex integer
     * @return array
     */
    public static function getDictionaryGrid($startIndex=self::START_INDEX, $endIndex=self::END_INDEX)
    {
        $dictionary = [];
        for ($i = $startIndex; $i <= $endIndex; $i++) {
            array_push($dictionary, ['letter'=>chr($i)]);
        }
        return $dictionary;
    }

    /**
     * @param $valueIndexes array
     * @param $keyIndexes array
     * @return array
     */
    public static function getComparedIndexes($valueIndexes, $keyIndexes)
    {
        $indexes = [];
        $keyLength = count($keyIndexes);
        $keyIterator = 0;
        for ($i = 0; $i < count($valueIndexes); $i++) {
            array_push($indexes, [$valueIndexes[$i], $keyIndexes[$keyIterator]]);
            $keyIterator++;
            if ($keyIterator >= $keyLength) {
                $keyIterator = 0;
            }
        }
        return $indexes;
    }

    /**
     * @param $text string
     * @param $dictionary array
     * @return array
     */
    public static function getIndexes($text, $dictionary)
    {
        $return = [];

        for ($i = 0; $i < strlen($text); $i++)
        {
            foreach ($dictionary as $index=>$char)
            {
                if ($text[$i]==$char)
                {
                    array_push($return, $index);
                }
            }
        }

        return $return;
    }

    /**
     * @param $valueIndexes array
     * @param $keyIndexes array
     * @param $dictionary array
     * @return array
     */
    public static function getEncodedIndexes($valueIndexes, $keyIndexes, $dictionary)
    {
        $comparedIndexes = self::getComparedIndexes($valueIndexes, $keyIndexes);

        $return = [];

        foreach ($comparedIndexes as $comparedIndex) {
            array_push($return, (($comparedIndex[0] + $comparedIndex[1]) % count($dictionary)));
        }

        return $return;
    }

    /**
     * @param $valueIndexes array
     * @param $keyIndexes array
     * @param $dictionary array
     * @return array
     */
    public static function getEncodedIndexesRevert($valueIndexes, $keyIndexes, $dictionary)
    {
        $comparedIndexes = self::getComparedIndexes($valueIndexes, $keyIndexes);

        $return = [];

        foreach ($comparedIndexes as $comparedIndex) {
            array_push($return, (($comparedIndex[0] - $comparedIndex[1] + count($dictionary)) % count($dictionary)));
        }

        return $return;
    }

    /**
     * @param $indexes array
     * @param $dictionary array
     * @return array
     */
    public static function getEncodedText($indexes, $dictionary)
    {
        $return = [];

        for ($i = 0; $i < count($indexes); $i++)
        {
            foreach ($dictionary as $index => $char)
            {
                if ($indexes[$i] == $index)
                {
                    array_push($return, $char);
                }
            }
        }

        return $return;
    }

    /**
     * @param $value string
     * @param $key string
     * @param $startIndex integer
     * @param $endIndex integer
     * @return array
     */
    public static function encode($value, $key, $startIndex=self::START_INDEX, $endIndex=self::END_INDEX)
    {
        $dictionary = self::getDictionary($startIndex, $endIndex);
        $valueIndexes = self::getIndexes($value, $dictionary);
        $keyIndexes = self::getIndexes($key, $dictionary);

        $encodedIndexes = self::getEncodedIndexes($valueIndexes, $keyIndexes, $dictionary);

        return self::getEncodedText($encodedIndexes, $dictionary);
    }

    /**
     * @param $value string
     * @param $key string
     * @param $startIndex integer
     * @param $endIndex integer
     * @return array
     */
    public static function decode($value, $key, $startIndex=self::START_INDEX, $endIndex=self::END_INDEX)
    {
        $dictionary = self::getDictionary($startIndex, $endIndex);
        $valueIndexes = self::getIndexes($value, $dictionary);
        $keyIndexes = self::getIndexes($key, $dictionary);

        $encodedIndexes = self::getEncodedIndexesRevert($valueIndexes, $keyIndexes, $dictionary);

        return self::getEncodedText($encodedIndexes, $dictionary);
    }

    /**
     * @param $min
     * @param $max
     * @return integer
     */
    public static function getChar($min = ComplexPasswordGenerator::ALPHABET_LATIN_START, $max = ComplexPasswordGenerator::ALPHABET_LATIN_END)
    {
        $random = mt_rand($min, $max);
        return chr($random);
    }

    /**
     * @param $int
     * @param $min
     * @param $max
     * @return boolean
     */
    public static function inRange($int,$min,$max)
    {
        return ($min<=$int && $int<=$max);
    }
}