<?php

namespace app\models;

use Yii;

/**
 * ComplexPasswordGenerator
 *
 * @property string[] $Modes
 * @property string[] $ModessLabels
 */
class Vigenere extends CommonModel
{
    public $value;
    public $key;
    public $mode;

    const MODE_ENCODE = 'encode';
    const MODE_DECODE = 'decode';

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['value', 'key', 'mode'], 'required'],
            [['value', 'key'], 'string', 'max' => 500],
            ['mode', 'in', 'range'=>$this->getModes()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'value' => 'Значение',
            'key' => 'Ключ',
            'mode' => 'Мод',
        ];
    }

    /**
     * @return array
     */
    public static function getModes()
    {
        return [
            self::MODE_ENCODE => self::MODE_ENCODE,
            self::MODE_DECODE => self::MODE_DECODE,
        ];
    }

    /**
     * @return array
     */
    public static function getModesLabels()
    {
        return [
            self::MODE_ENCODE => 'Шифровать',
            self::MODE_DECODE => 'Дешифровать',
        ];
    }

    /**
     * @return array
     */
    public function calculate()
    {
        switch ($this->mode)
        {
            case self::MODE_ENCODE:
                $return = self::encode($this->value, $this->key);
                break;
            case self::MODE_DECODE:
                $return = self::decode($this->value, $this->key);
                break;
            default:
                $return = array();
        }
        return $return;
    }
}