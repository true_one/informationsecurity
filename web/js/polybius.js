/**
 * Created by khan_alexander on 10.03.2016.
 */
(function(){
    $(function(){
        if ($("#encode").length==0){
            return;
        }

        var charmap = {};
        var rus = "йцукенгшщзхъфывапролджэячсмитьбюЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ,".split('');
        var eng = "qwertyuiop[]asdfghjkl;'zxcvbnm,.QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>?".split('');
        for (var i = 0; i < eng.length; i++) {
            charmap[eng[i]] = rus[i];
        }

        function engtorus(string) {
            return string.replace(/([^а-яё\s])/gi,

                function (x) {
                    return charmap[x] || x;
                });
        }

        var kv = [
            ["А", "Б", "В", "Г", "Д", "Е"],
            ["Ё", "Ж", "З", "И", "Й", "К"],
            ["Л", "М", "Н", "О", "П", "Р"],
            ["С", "Т", "У", "Ф", "Х", "Ц"],
            ["Ч", "Ш", "Щ", "Ъ", "Ы", "Ь"],
            ["Э", "Ю", "Я", ",", ".", " "]
        ];

        function pol_encode(str) {
            var idx;
            var res = "";
            str = str.toUpperCase();
            for (var j = 0; j < str.length; j++) {
                for (var i = 0; i < kv.length; i++) {
                    idx = kv[i].indexOf(str.charAt(j));
                    if (idx >= 0) {
                        res += " " + i + "" + idx;
                        break;
                    }
                }
            }
            return $.trim(res);
        }

        function  pol_decode (str) {
            var row, col;
            var res = "";
            var ar = str.split(" ");
            for (var j = 0; j < ar.length; j++) {
                if (ar[j].length == 2) {
                    row = parseInt(ar[j].charAt(0));
                    col = parseInt(ar[j].charAt(1));
                    res += kv[row][col];
                }
            }
            return res;
        }
        $("#encode").on('input keydown paste', function() {
            var newval = engtorus($(this).val());
            if ($(this).val() != newval) {
                $(this).val(newval);
            }
            $("#decode").val( pol_encode ($("#encode").val()));
        });

        $("#decode").on('input keydown paste', function() {
            var newval = engtorus($(this).val());
            if ($(this).val() != newval) {
                $(this).val(newval);
            }
            $("#encode").val(  pol_decode ($("#decode").val()));
        });

        $("#decode").val( pol_encode ($("#encode").val()));
    })
}).call(this);

