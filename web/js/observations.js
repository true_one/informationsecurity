/**
 * Created by khan_alexander on 16.03.2016.
 */

(function(){
    $(function(){
        var form, count, observations;
        form = $('.form-math');
        if (form.length==0){
            return;
        }
        count = form.find('#mathgenerator-count');
        observations = form.find('.observations');

        count.on('change', function(){
            observations.empty();
        });
    })
}).call(this);

