<?php

namespace app\controllers;

use app\models\Vigenere;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\PasswordGenerator;
use app\models\ComplexPasswordGenerator;
use app\models\MathGenerator;
use app\models\ComplexMathGenerator;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $links = [
            ['label'=>'Тусупова С.А.', 'url'=>'site/tusupova'],
            ['label'=>'Кусаинова А.С.', 'url'=>'site/kusainova'],
        ];
        return $this->render('index', [
            'links'=>$links,
        ]);
    }

    public function actionTusupova()
    {
        $links = [
            ['label'=>'Лабораторная №1', 'url'=>'site/pgen'],
            ['label'=>'Лабораторная №2', 'url'=>'site/username'],
            ['label'=>'Шифрование методом Полибия', 'url'=>'site/polybius'],
            ['label'=>'Шифрование методом Виженера', 'url'=>'site/vigenere'],
        ];
        return $this->render('tusupova',[
            'links'=>$links,
        ]);
    }

    public function actionKusainova()
    {
        $links = [
            ['label'=>'Рубежка №1', 'url'=>'math'],
            ['label'=>'Рубежка №2', 'url'=>'complex']
        ];
        return $this->render('kusainova', [
            'links'=>$links,
        ]);
    }

    // ----------TUSUPOVA----------
    public function actionPgen()
    {
        $model = new PasswordGenerator();
        $model->load(Yii::$app->request->post());
        return $this->render('pgen', [
            'model' => $model,
        ]);
    }

    public function actionUsername()
    {
        $model = new ComplexPasswordGenerator();
        $model->load(Yii::$app->request->post());
        return $this->render('username', [
            'model' => $model,
        ]);
    }

    public function actionPolybius()
    {
        return $this->render('polybius');
    }

    public function actionVigenere()
    {
        $model = new Vigenere();
        $model->load(Yii::$app->request->post());
        return $this->render('vigenere', [
            'model'=>$model,
        ]);
    }
    // ----------TUSUPOVA----------

    // ----------KUSAINOVA----------
    public function actionMath()
    {
        $model = new MathGenerator();
        $model->load(Yii::$app->request->post());
        if (isset($_POST['MathObservation']))
        {
            $model->mathObservations = $_POST['MathObservation'];
        }
        return $this->render('math', [
            'model' => $model,
        ]);
    }

    public function actionComplex()
    {
        $model = new ComplexMathGenerator();
        $model->load(Yii::$app->request->post());
        if (isset($_POST['ComplexMathObservation']))
        {
            $model->mathObservations = $_POST['ComplexMathObservation'];
        }
        return $this->render('complex', [
            'model' => $model,
        ]);
    }
    // ----------KUSAINOVA----------

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }
}
