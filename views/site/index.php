<?php

/* @var $this yii\web\View */
/* @var $links */

use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'Выполненые задания';
?>
<div class="site-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php foreach($links as $link) : ?>
        <p><?= Html::a($link['label'], Url::toRoute($link['url']))?></p>
    <?php endforeach; ?>
</div>
