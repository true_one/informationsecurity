<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ComplexMathGenerator */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;
use yii\widgets\ListView;

$this->title = 'Рубежка №2';
$this->params['breadcrumbs'][] = ['label'=>'Кусаинова А.С.', 'url'=>['site/kusainova']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <h1 class="col-lg-offset-2"><?= Html::encode($this->title) ?></h1>
    <p class="col-lg-offset-2"><?= Html::a('Новый расчет', Url::toRoute('site/complex'))?></p>
    <p class="col-lg-offset-2"><i>Примечание: вещественные числа вводить с точкой (.), а не с запятой (,)</i></p>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-math form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-offset-2 col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>
    <?php if (!empty($model->mathObservations)): ?>
        <?php $model->calculate(); ?>

        <div class="col-lg-offset-2">
            <p>Среднее знач. Y = <?= $model->yMiddle; ?></p>
            <p>Среднее знач. X1 = <?= $model->x1Middle; ?></p>
            <p>Среднее знач. X2 = <?= $model->x2Middle; ?></p>
        </div>
        <?php if (!empty($model->xHorizontal) && !empty($model->xVertical)) : ?>
            <div class="col-lg-offset-2">
                <h4>Транспортированная Х</h4>
                <table class="table table-striped table-bordered">
                    <?php foreach ($model->xHorizontal as $xHorizontal) : ?>
                        <tr>
                            <?php foreach ($xHorizontal as $x): ?>
                                <td><?= $x; ?></td>
                            <?php endforeach;?>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <table class="table table-striped table-bordered">
                    <?php foreach ($model->xVertical as $xVertical) : ?>
                        <tr>
                            <?php foreach ($xVertical as $x): ?>
                                <td><?= $x; ?></td>
                            <?php endforeach;?>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <p>Сумма x1 = <?= $model->mathObservationsSum['sumx1']; ?></p>
                <p>Сумма x2 = <?= $model->mathObservationsSum['sumx2']; ?></p>
            </div>
        <?php endif; ?>
        <?php if (!empty($model->xtx)): ?>
            <div class="col-lg-offset-2">
                <h4>Транспортированная Х на Х</h4>
                <table class="table table-striped table-bordered">
                    <?php foreach ($model->xtx as $xtxArray) : ?>
                        <tr>
                            <?php foreach ($xtxArray as $xtx): ?>
                                <td><?= $xtx; ?></td>
                            <?php endforeach;?>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <p>Детерминант(определитель) = <?= $model->determinant;?></p>

                <h4>Алгебраические дополнения к элементам матрицы А</h4>
                <table class="table table-striped table-bordered">
                    <tr>
                        <td>A(11) = <?= $model->a11; ?></td>
                        <td>A(21) = <?= $model->a21; ?></td>
                        <td>A(31) = <?= $model->a31; ?></td>
                    </tr>
                    <tr>
                        <td>A(12) = <?= $model->a12; ?></td>
                        <td>A(22) = <?= $model->a22; ?></td>
                        <td>A(32) = <?= $model->a32; ?></td>
                    </tr>
                    <tr>
                        <td>A(13) = <?= $model->a13; ?></td>
                        <td>A(23) = <?= $model->a23; ?></td>
                        <td>A(33) = <?= $model->a33; ?></td>
                    </tr>
                </table>
                <h4>Матрица коэффициентов системы уравнения</h4>
                <p>A(ij) - союзная матрица</p>
                <table class="table table-striped table-bordered">
                    <?php foreach ($model->aArray as $array) : ?>
                        <tr>
                            <?php foreach ($array as $value): ?>
                                <td><?= $value; ?></td>
                            <?php endforeach;?>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <p>A-1 - обратная матрица = (1/детерминант) * A(ij) = <?= $model->determinantCoeff;?> * A(ij)</p>
                <table class="table table-striped table-bordered">
                    <?php foreach ($model->aArrayRevert as $array) : ?>
                        <tr>
                            <?php foreach ($array as $value): ?>
                                <td><?= $value; ?></td>
                            <?php endforeach;?>
                        </tr>
                    <?php endforeach; ?>
                </table>
                <p>Xt*Y</p>
                <table class="table table-striped table-bordered">
                    <?php foreach ($model->xty as $value): ?>
                        <tr>
                            <td><?= $value; ?></td>
                        </tr>
                    <?php endforeach;?>
                </table>
                <p>A = A-1 * (Xt*Y)</p>
                <table class="table table-striped table-bordered">
                    <?php foreach ($model->aValues as $value): ?>
                        <tr>
                            <td><?= round($value, 2); ?></td>
                        </tr>
                    <?php endforeach;?>
                </table>
                <p>Расчетное значение Y(t) = транспортированная x * A</p>
                <table class="table table-striped table-bordered">
                    <?php foreach ($model->yt as $value): ?>
                        <tr>
                            <td><?= round($value, 2); ?></td>
                        </tr>
                    <?php endforeach;?>
                </table>
                <p>E отклонение</p>
                <table class="table table-striped table-bordered">
                    <?php foreach ($model->e as $value): ?>
                        <tr>
                            <td><?= round($value, 2); ?></td>
                        </tr>
                    <?php endforeach;?>
                </table>
                <p>A = </p>
                <table class="table table-striped table-bordered">
                    <?php foreach ($model->a as $value): ?>
                        <tr>
                            <td><?= $value; ?></td>
                        </tr>
                    <?php endforeach;?>
                </table>
                <p>Среднее = <?= $model->aMiddle; ?></p>
                <h4>Э1 = <?= $model->el1; ?></h4>
                <h4>Э2 = <?= $model->el2; ?></h4>
                <p>Детерминанты — это факторы оказывающие воздействие на спрос или предложение.</p>
            </div>
        <?php endif; ?>
    <?php else :?>
        <?= $form->field($model, 'count')->textInput() ?>

        <?php if ($model->validate()) : ?>
            <div class="observations">
                <?php for ($i=0; $i<$model->count; $i++) :?>
                    <?php $observation = new app\models\ComplexMathObservation(); ?>

                    <?= $form->field($observation, "[{$i}]y")->textInput()->label($i+1 . ") y"); ?>
                    <?= $form->field($observation, "[{$i}]x1")->textInput()->label($i+1 . ") x1"); ?>
                    <?= $form->field($observation, "[{$i}]x2")->textInput()->label($i+1 . ") x2"); ?>
                <?php endfor; ?>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-11">
                <?= Html::submitButton('Далее', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>

</div>
