<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ComplexPasswordGenerator */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$this->title = 'Генератор Паролей';
$this->params['breadcrumbs'][] = ['label'=>'Тусупова С.А.', 'url'=>['site/tusupova']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

  <h1 class="col-lg-offset-2"><?= Html::encode($this->title) ?></h1 class="col-lg-offset-2">

  <?php $form = ActiveForm::begin([
      'id' => 'login-form',
      'options' => ['class' => 'form-horizontal'],
      'fieldConfig' => [
          'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-offset-2 col-lg-8\">{error}</div>",
          'labelOptions' => ['class' => 'col-lg-2 control-label'],
      ],
  ]); ?>

    <?= $form->field($model, 'username')->textInput() ?>

    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-11">
            <?= Html::submitButton('Сгенерировать', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php if ($model->validate()) : ?>
        <?php
        $provider = new ArrayDataProvider([
            'allModels' =>  $model->calculate(),
        ]);
        echo GridView::widget([
            'dataProvider' => $provider,
            'options'=>['class'=>'col-lg-offset-2'],
            'columns' => [
                [
                    'attribute' => 'variant',
                    'label' => 'Вариант',
                ],
                [
                    'attribute' => 'length',
                    'label' => 'Кол-во символов',
                ],
                [
                    'attribute' => 'description',
                    'format' => 'html',
                    'label' => 'Описание',
                ],
                [
                    'attribute' => 'password',
                    'label' => 'Описание',
                ],
//                [
//                    'attribute' => 'S',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['S']);
//                        }
//                ],
//                [
//                    'attribute' => 'generatedPassword',
//                    'label' => 'Пароли',
//                    'format' => 'html',
//                    'value' => function($data){
//                            $return = '';
//                            foreach ($data['generatedPassword'] as $password)
//                            {
//                                $return .= $password . "<br/>";
//                            }
//                            return $return;
//                        }
//                ],
            ],
        ]);
        ?>
    <?php endif; ?>
  <?php ActiveForm::end(); ?>

</div>
