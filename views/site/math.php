<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\MathGenerator */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$this->title = 'Рубежка №1';
$this->params['breadcrumbs'][] = ['label'=>'Кусаинова А.С.', 'url'=>['site/kusainova']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">

    <h1 class="col-lg-offset-2"><?= Html::encode($this->title) ?></h1>
    <p class="col-lg-offset-2"><?= Html::a('Новый расчет', Url::toRoute('site/math'))?></p>
    <p class="col-lg-offset-2"><i>Примечание: вещественные числа вводить с точкой (.), а не с запятой (,)</i></p>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-math form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-offset-2 col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>

    <?php if (!empty($model->mathObservations)): ?>
        <?php
        $model->calculate();
        $tfirst = new ArrayDataProvider([
            'allModels' =>  $model->mathObservations,
            'pagination' => [
                'pageSize' => 1000,
            ],
        ]);
        $tsecond = new ArrayDataProvider([
            'allModels' =>  [$model->mathObservationsSum],
            'pagination' => [
                'pageSize' => 1000,
            ],
        ]);
        $tthird = new ArrayDataProvider([
            'allModels' =>  [$model->mathObservationsDeltas],
            'pagination' => [
                'pageSize' => 1000,
            ],
        ]);
        echo GridView::widget([
            'dataProvider' => $tfirst,
            'options'=>['class'=>'col-lg-offset-2'],
            'columns' => [
                [
                    'attribute' => 'x',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['x']);
//                        },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'y',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['y']);
//                        },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'xy',
                    'label' => 'XY',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['xy']);
//                        },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'x2',
                    'label' => 'X^2',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['x2']);
//                        },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'yx',
                    'label' => 'Y(X)',
//                    'value' => function($data){
//                        return \Yii::$app->formatter->asDecimal($data['yx']);
//                    },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'e',
//                    'value' => function($data){
//                        return \Yii::$app->formatter->asDecimal($data['e']);
//                    },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'module',
                    'label' => 'Модуль',
//                    'value' => function($data){
//                        return \Yii::$app->formatter->asDecimal($data['module']);
//                    },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
            ],
        ]);
        echo GridView::widget([
            'dataProvider' => $tsecond,
            'options'=>['class'=>'col-lg-offset-2'],
            'columns' => [
                [
                    'attribute' => 'sumx',
                    'label' => 'Сумма X',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['sumx']);
//                        },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'sumy',
                    'label' => 'Сумма Y',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['sumy']);
//                        },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'avx',
                    'label' => 'Ср. знач. X',
//                    'value' => function($data){
//                        return \Yii::$app->formatter->asDecimal($data['avx']);
//                    },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'avy',
                    'label' => 'Ср. знач. Y',
//                    'value' => function($data){
//                        return \Yii::$app->formatter->asDecimal($data['avy']);
//                    },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'sumxy',
                    'label' => 'Сумма XY',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['sumxy']);
//                        },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'sumx2',
                    'label' => 'Сумма X^2',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['sumx2']);
//                        },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
                [
                    'attribute' => 'avmodule',
                    'label' => 'Ср. знач. модуля',
//                    'value' => function($data){
//                        return \Yii::$app->formatter->asDecimal($data['avmodule']);
//                    },
                    'contentOptions'=>['style'=>'width: 14%;'],
                ],
            ],
        ]);
        echo GridView::widget([
            'dataProvider' => $tthird,
            'options'=>['class'=>'col-lg-offset-2'],
            'columns' => [
                [
                    'attribute' => 'delta',
                    'label' => 'Дельта',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['delta']);
//                        },
                    'contentOptions'=>['style'=>'width: 16%;'],
                ],
                [
                    'attribute' => 'adelta',
                    'label' => 'Дельта A',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['adelta']);
//                        },
                    'contentOptions'=>['style'=>'width: 16%;'],
                ],
                [
                    'attribute' => 'bdelta',
                    'label' => 'Дельта B',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['bdelta']);
//                        },
                    'contentOptions'=>['style'=>'width: 16%;'],
                ],
                [
                    'attribute' => 'a',
                    'label' => 'A',
//                    'value' => function($data){
//                            return \Yii::$app->formatter->asDecimal($data['a']);
//                        },
                    'contentOptions'=>['style'=>'width: 16%;'],
                ],
                [
                    'attribute' => 'b',
                    'label' => 'B',
//                    'value' => function($data){
//                        return \Yii::$app->formatter->asDecimal($data['b']);
//                    },
                    'contentOptions'=>['style'=>'width: 16%;'],
                ],
                [
                    'attribute' => 'elastic',
                    'label' => 'Коэф. эластичности',
//                    'value' => function($data){
//                        return \Yii::$app->formatter->asDecimal($data['elastic']);
//                    },
                    'contentOptions'=>['style'=>'width: 16%;'],
                ],
            ],
        ]);
        ?>
    <?php else :?>
        <?= $form->field($model, 'count')->textInput() ?>


        <?php if ($model->validate()) : ?>
            <div class="observations">
                <?php for ($i=0; $i<$model->count; $i++) :?>
                    <?php $observation = new app\models\MathObservation(); ?>

                    <?= $form->field($observation, "[{$i}]x")->textInput()->label("X{$i}"); ?>
                    <?= $form->field($observation, "[{$i}]y")->textInput()->label("Y{$i}"); ?>
                <?php endfor; ?>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-11">
                <?= Html::submitButton('Далее', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>
        </div>
    <?php endif; ?>
    <?php ActiveForm::end(); ?>

</div>
