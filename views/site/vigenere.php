<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\Vigenere */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\data\ArrayDataProvider;
use yii\grid\GridView;

$this->title = 'Шифрование методом Виженера';
$this->params['breadcrumbs'][] = ['label'=>'Тусупова С.А.', 'url'=>['site/tusupova']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <h1 class="col-lg-offset-2"><?= Html::encode($this->title) ?></h1>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-offset-2 col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-2 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'value')->textInput() ?>
    <?= $form->field($model, 'key')->textInput() ?>
    <?= $form->field($model, 'mode')->radioList($model->ModesLabels) ?>

    <div class="form-group">
        <div class="col-lg-offset-2 col-lg-11">
            <?= Html::submitButton('Сгенерировать', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php if ($model->validate()) : ?>

        <div class="form-group">
            <?= Html::label('Результат', 'result', ['class'=>'col-lg-2 control-label']) ?>
            <div class="col-lg-3">
                <?= Html::input('text', 'result', implode('', $model->calculate()), ['class'=>'form-control'])?>
            </div>
        </div>

    <?php endif; ?>

    <div class="form-group">
        <?= Html::label('Таблица используемых символов', '', ['class'=>'col-lg-2 control-label']) ?>
        <div class="col-lg-3">
            <?php
            $provider = new ArrayDataProvider([
                'allModels' =>  $model->getDictionaryGrid(),
                'pagination' => [
                    'pageSize' => 1000,
                ],
            ]);
            echo GridView::widget([
                'dataProvider' => $provider,
                'options'=>['class'=>''],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'letter',
                        'label' => 'Символ',
                        'value' => function($data){
                            return '\''.$data['letter'].'\'';
                        }
                    ],
                ],
            ]);
            ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

<?php
/*
const SPACING_X   = 1;
const SPACING_Y   = 0;
const JOINT_CHAR  = '+';
const LINE_X_CHAR = '-';
const LINE_Y_CHAR = '|';

$table = array(
    array(
        'Name'    => 'Trixie',
        'Color'   => 'Green',
        'Element' => 'Earth',
        'Likes'   => 'Flowers'
    ),
    array(
        'Name'    => 'Tinkerbell',
        'Element' => 'Air',
        'Likes'   => 'Singing',
        'Color'   => 'Blue'
    ),
    array(
        'Element' => 'Water',
        'Likes'   => 'Dancing',
        'Name'    => 'Blum',
        'Color'   => 'Pink'
    ),
);

function draw_table($table)
{

    $nl              = "\n";
    $columns_headers = columns_headers($table);
    $columns_lengths = columns_lengths($table, $columns_headers);
    $row_separator   = row_seperator($columns_lengths);
    $row_spacer      = row_spacer($columns_lengths);
    $row_headers     = row_headers($columns_headers, $columns_lengths);

    echo '<pre>';

    echo $row_separator . $nl;
    echo str_repeat($row_spacer . $nl, SPACING_Y);
    echo $row_headers . $nl;
    echo str_repeat($row_spacer . $nl, SPACING_Y);
    echo $row_separator . $nl;
    echo str_repeat($row_spacer . $nl, SPACING_Y);
    foreach ($table as $row_cells) {
        $row_cells = row_cells($row_cells, $columns_headers, $columns_lengths);
        echo $row_cells . $nl;
        echo str_repeat($row_spacer . $nl, SPACING_Y);
    }
    echo $row_separator . $nl;

    echo '</pre>';

}

function columns_headers($table)
{
    return array_keys(reset($table));
}

function columns_lengths($table, $columns_headers)
{
    $lengths = [];
    foreach ($columns_headers as $header) {
        $header_length = strlen($header);
        $max           = $header_length;
        foreach ($table as $row) {
            $length = strlen($row[$header]);
            if ($length > $max) {
                $max = $length;
            }
        }

        if (($max % 2) != ($header_length % 2)) {
            $max += 1;
        }

        $lengths[$header] = $max;
    }

    return $lengths;
}

function row_seperator($columns_lengths)
{
    $row = '';
    foreach ($columns_lengths as $column_length) {
        $row .= JOINT_CHAR . str_repeat(LINE_X_CHAR, (SPACING_X * 2) + $column_length);
    }
    $row .= JOINT_CHAR;

    return $row;
}

function row_spacer($columns_lengths)
{
    $row = '';
    foreach ($columns_lengths as $column_length) {
        $row .= LINE_Y_CHAR . str_repeat(' ', (SPACING_X * 2) + $column_length);
    }
    $row .= LINE_Y_CHAR;

    return $row;
}

function row_headers($columns_headers, $columns_lengths)
{
    $row = '';
    foreach ($columns_headers as $header) {
        $row .= LINE_Y_CHAR . str_pad($header, (SPACING_X * 2) + $columns_lengths[$header], ' ', STR_PAD_BOTH);
    }
    $row .= LINE_Y_CHAR;

    return $row;
}

function row_cells($row_cells, $columns_headers, $columns_lengths)
{
    $row = '';
    foreach ($columns_headers as $header) {
        $row .= LINE_Y_CHAR . str_repeat(' ', SPACING_X) . str_pad($row_cells[$header], SPACING_X + $columns_lengths[$header], ' ', STR_PAD_RIGHT);
    }
    $row .= LINE_Y_CHAR;

    return $row;
}

draw_table($table);*/
?>
</div>
