<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Квадрат Полибия';
$this->params['breadcrumbs'][] = ['label'=>'Тусупова С.А.', 'url'=>['site/tusupova']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        Пример реализации шифрования методом Полибия. <br>
        <strong>Шифрование</strong> происходит при вводе текста в <strong>первое</strong> поле и выводится во <strong>втором</strong> поле. <br>
        <strong>Дешифрование</strong> происодит при вводе текста во <strong>второе</strong> поле и выводится в <strong>первом</strong> поле. <br>
        <i>Вводить в первое поле можно только русские символы. Допускается ввод пробела, точки или запятой. </i>
    </p>
    <?= Html::beginForm('', 'post', ['class'=>'form-horizontal']); ?>
        <div class="form-group">
            <div class="col-lg-7">
                <?= Html::input('text', 'str', 'Хэшкод', ['id'=>'encode', 'class'=>'form-control'])?>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-7">
                <?= Html::input('text', 'resstr', '', ['id'=>'decode', 'class'=>'form-control'])?>
            </div>
        </div>
    <?= Html::endForm(); ?>

</div>
